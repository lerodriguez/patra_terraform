#ECS cloudwatsh dashboard
resource "aws_cloudwatch_dashboard" "cloudwatch_dashboard" {
  dashboard_name = "${var.stack_id}-${var.env}-fargate"
  dashboard_body = data.template_file.dashboard-template.rendered
}

# Templates
data "template_file" "dashboard-template" {
  template = file("dashboard.json")
  vars = {
    service-name = aws_ecs_service.python-app.name
    cluster-name = aws_ecs_cluster.cluster.name
    target-group = aws_alb_target_group.python-app-tg.arn_suffix
    load-balancer = aws_alb.python-app-lb.arn_suffix
    region = var.region
  }
}

data "aws_caller_identity" "current" {}

# CloudWatch scaling alarms
resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name          = "${var.stack_id}-${var.env}-CPU-high-${var.ecs_max_load_percentage}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.ecs_max_load_percentage

  dimensions = {
    ClusterName = aws_ecs_cluster.cluster.name
    ServiceName = aws_ecs_service.python-app.name
  }

  alarm_actions = [aws_appautoscaling_policy.scale_up.arn]
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization_low" {
  alarm_name          = "${var.stack_id}-${var.env}-CPU-Low-${var.ecs_min_load_percentage}"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.ecs_min_load_percentage

  dimensions = {
    ClusterName = aws_ecs_cluster.cluster.name
    ServiceName = aws_ecs_service.python-app.name
  }

  alarm_actions = [aws_appautoscaling_policy.scale_down.arn]
}
