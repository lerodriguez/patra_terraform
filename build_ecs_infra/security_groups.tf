#LB SG
resource "aws_security_group" "python-app-lb-sg" {
  name        = "${var.stack_id}-${var.env}-python-app-lb-sg"
  description = "Restrict/allow access"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Task definition SG
resource "aws_security_group" "python-app-task" {
  name        = "${var.stack_id}-python-app-task"
  description = "allow inbound access from the ALB only"
  vpc_id      =  data.aws_vpc.default.id

  ingress {
    protocol        = "tcp"
    from_port       = var.app_port
    to_port         = var.app_port
    security_groups = [aws_security_group.python-app-lb-sg.id]
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.app_port
    protocol = "tcp"
    to_port = var.app_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
