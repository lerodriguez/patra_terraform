###############################################################
#Task Definitions
###############################################################
#aepa
resource "aws_ecs_task_definition" "python-app" {
  family                   = "python-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn
  container_definitions = <<DEFINITION
  [
    {
      "cpu": ${var.fargate_cpu},
      "image": "${var.app_image}:${var.commit_id}",
      "memory": ${var.fargate_memory},
      "name": "python-app",
      "networkMode": "awsvpc",
      "portMappings": [
        {
          "containerPort": ${var.app_port},
          "hostPort": ${var.app_port}
        }
      ],
      "logConfiguration": {
                  "logDriver": "awslogs",
                  "options": {
                      "awslogs-group": "${aws_cloudwatch_log_group.awslogs-python-app.name}",
                      "awslogs-region": "${var.region}",
                      "awslogs-stream-prefix": "python-app"
                  }
        }
     }
 ]
DEFINITION
}
