#CPU units (1 vCPU = 1024 CPU units)
fargate_cpu = "512"

#Memory in MB
fargate_memory = "1024"

#App port
app_port = "8080"

#App Count
app_count = "2"

#App ECR Image
app_image = "479817904450.dkr.ecr.us-east-1.amazonaws.com/python_app"

#AutoScaling configuration
minimum_autoscale_instances   =  2
maximum_autoscale_instances   =  6
log-retention		      =  3
ecs_max_load_percentage       = 80
ecs_min_load_percentage       = 30
