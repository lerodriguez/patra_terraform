#S3 bucket for lb logs
resource "aws_s3_bucket" "alb" {
bucket = "python-app-alb-logs"
force_destroy = true
#LB Account ID not the AWS account.
#https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-access-logs.html
policy = <<EOF
{
  "Version": "2012-10-17",
    "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
      "AWS": "arn:aws:iam::127311923021:root"
    },
    "Action": "s3:PutObject",
    "Resource": "arn:aws:s3:::python-app-alb-logs/${var.env}/*"
    }
  ]
}
EOF
}
