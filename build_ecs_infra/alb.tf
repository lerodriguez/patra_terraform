# ALB - Application Load Balancer
resource "aws_alb" "python-app-lb" {
  name = "${var.stack_id}-${var.env}-lb"
  load_balancer_type = "application"
  enable_cross_zone_load_balancing = true
  internal = false
  subnets =  data.aws_subnet_ids.subnet_ids.ids
  security_groups = [ aws_security_group.python-app-lb-sg.id ]
  access_logs {
    bucket  = aws_s3_bucket.alb.bucket
    prefix  = var.env
    enabled = true
  }
  tags = {
    Environment = upper(var.env)
  }
}

resource "aws_alb_target_group" "python-app-tg" {
  name        = "${var.stack_id}-python-app-lb-tg"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id
  target_type = "ip"
  health_check {
    path = "/"
    port = 8080
    protocol = "HTTP"
    healthy_threshold = 5
    unhealthy_threshold = 3
    timeout = 2
    interval = 5
    matcher = "200"  # has to be HTTP 200 or fails
  }
}

# ALB Listeners
resource "aws_alb_listener" "app-endpoint" {
  load_balancer_arn = aws_alb.python-app-lb.id
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.python-app-tg.id
    type             = "forward"
  }
}
