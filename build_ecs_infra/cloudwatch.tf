#CloudWatch logs for task
resource "aws_cloudwatch_log_group" "awslogs-python-app" {
  name = "awslogs-${var.stack_id}-python-app"

  tags = {
    Environment = var.env
  }
}
