#Cluster definition
resource "aws_ecs_cluster" "cluster" {
  name = "${var.stack_id}-${var.env}"
  tags = {
    Environment = upper(var.env)
  }
}

resource "aws_ecs_service" "python-app" {
  name            = "python-app"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.python-app.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"
  
  network_configuration {
    security_groups  = [ aws_security_group.python-app-task.id ]
    subnets          = data.aws_subnet_ids.subnet_ids.ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.python-app-tg.id
    container_name   = "python-app"
    container_port   = var.app_port
  }

  depends_on = [
    aws_alb_listener.app-endpoint,
  ]
}


